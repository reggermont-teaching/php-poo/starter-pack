<?php

// cast magic barrier

require_once '../exercises/exercise.class.php';

class TP 
{
    const EXERCISES = 11;

    private function exercise1(): string 
    {
        return 'Exercise 1 done';
    }

    private function exercise2(): ?string 
    {
        $firstname = 'yVEs';
        $lastname = 'skRZyPCZyK';

        // end magic barrier

        $firstname = ucfirst(strtolower($firstname));
        $lastname = strtoupper($lastname);

        $result =  $firstname ." ". $lastname;

        // cast magic barrier

        return $result;
    }

    private function exercise3(): ?string 
    {   
        $hackedText = 'Y0u\'v3 b33n h4ck3d';

        // end magic barrier

        $search = ['0', '3', '4'];
        $replace = ['o', 'e', 'a'];

        $result = str_replace($search, $replace, $hackedText);

        // cast magic barrier

        return $result;
    }

    private function exercise4(): ?string 
    {
        // end magic barrier

        $text=$this->exercise3();
        $characters = str_replace(' ','',$text);

        $result = 'Words: ' . str_word_count($text,0) . ' ; Characters: ' . strlen($characters);

        // cast magic barrier

        return $result;
    }

    private function exercise5(): ?string 
    {
        $ages = [16,47,32,-5,1003,24];

        // end magic barrier

        $result = '';

        for($i = 0; $i < count($ages); $i++) {
            $result .= $ages[$i] . ' is ' .
                ($ages[$i] >= 0 && $ages[$i] <= 100
                    ? ''
                    : 'not' )
                . 'a valid age';
            if ($i < (count($ages)-1)) {
                $result .= '<br>';
            }
        }

        /* Another solution:
         * foreach($ages as $age) {
         *     $result .= $age . ' is ' .
         *         ($age >= 0 && $age <= 100)
         *             ? ''
         *             : 'not'
         *         . 'a valid age' . '<br>';
         * }
         *
         * $result = rtrim($result, '<br>');
         */

        // cast magic barrier

        return $result;
    }

    private function exercise6(): ?string 
    {
        // end magic barrier

        $result = date('Y-m-d, H:i');

        // cast magic barrier

        return $result;
    }

    private function exercise7(): ?string 
    {
        // end magic barrier

        $result = (new Exercise('7'))->toString();

        // cast magic barrier

        return $result;
    }

    private function exercise8switch($fruit): ?string 
    {
        // end magic barrier

        $fruit = ucfirst(strtolower($fruit));

        $result = $fruit;

        switch($fruit) {
            case 'Orange':
                $result .= ' is orange';
                break;
            case 'Lemon':
                $result .= ' is yellow';
                break;
            case 'Raspberry':
                $result .= ' is pink';
                break;
            default:
                $result .= ': Fruit not found';
        }

        // cast magic barrier

        return $result;
    }

    private function exercise8(): string
    {
        return $this->exercise8switch('raspberry') 
            . '<br>'
            . $this->exercise8switch('leMOn')
            . '<br>'
            . $this->exercise8switch('apple');
    }

    private function exercise9noswitch($fruit): ?string 
    {
        // end magic barrier

        $fruit = ucfirst(strtolower($fruit));

        $mapping = [
            'Orange' => 'orange',
            'Lemon' => 'yellow',
            'Raspberry' => 'pink',
        ];

        $result = array_key_exists($fruit, $mapping)
            ? $fruit . ' is ' . $mapping[$fruit]
            : $fruit . ': Fruit not found';

        // cast magic barrier

        return $result;
    }

    private function exercise9(): string
    {
        return $this->exercise9noswitch('raspberry') 
            . '<br>'
            . $this->exercise9noswitch('lemon')
            . '<br>'
            . $this->exercise9noswitch('apple');
    }

    private function exercise10(): ?string
    {
        // end magic barrier

        $fruit = $_GET["fruit"];
        
        $result = $fruit 
            ? $this->exercise9noswitch($fruit)
            : "No 'fruit' query parameter found";

        // cast magic barrier

        return $result;
    }

    private function exercise11(): ?string
    {
        // end magic barrier



        // cast magic barrier

        return $result;
    }

    /**
     * @param   int         $exerciseNumber Number of the exercise function to call
     * @return  string|null 
     */
    private function getExerciseResult(int $exerciseNumber): ?string {
        $exerciseClass = 'exercise' . $exerciseNumber;

        $exerciseResult = $this->{$exerciseClass}();
        return (isset($exerciseResult) && !empty($exerciseResult))

            ? '========' . '<br>' .
                'Exercise ' . $exerciseNumber . ' start' . '<br>' .
                $exerciseResult . '<br>' .
                'Exercise ' . $exerciseNumber . ' end' . '<br>' .
                '========' . '<br><br>' 
            : null;
    }

    /**
     * @return  string 
     */
    public function toString(): string {
        $exercisesResult = '';

        for($i = 1; $i <= self::EXERCISES; $i++) {
            $exercisesResult .= $this->getExerciseResult($i);
        }


        return $exercisesResult;
    }
}