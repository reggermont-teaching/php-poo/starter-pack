<?php

class Exercise
{
    private $exerciseNumber;

    public function __construct(string $number) 
    {
        $this->exerciseNumber = $number;
    }

    /**
     * @return  string Details of the object
     */
    public function toString(): string {
        return 'Exercise ' . $this->$exerciseNumber . ' ok';
    }
}
