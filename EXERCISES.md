# Exercises

All exercises have to be done in the `exercises/exercises.class.php`

## Exercise 1

### Description

Access to the index page. For those who have docker: `make up` and go to [localhost](http://localhost)

### Expected Result

```text
Exercise 1 done
```


## Exercise 2

### Description

Format the text to display the firstname and lastname correctly

### Expected Result

```text
Yves SKRZYPCZYK
```


## Exercise 3

### Description

Oh no, the text provided for the exercise has been written by a dumb kid, please fix it to learn him how to write correctly and without any smiley

### Expected Result

```text
You've been hacked
```


## Exercise 4

### Description

Thank you for fixing the text. Please double-check that everything is alright : the sentence should have 3 words and 16 characters

### Expected Result

```text
Words: 3 ; Characters: 16
```


## Exercise 5

### Description

Check that the age is valid (between 0 and 100)

### Expected Result

```text
16 is a valid age
47 is a valid age
32 is a valid age
-5 is not a valid age
1003 is not a valid age
24 is a valid age
```


## Exercise 6

### Description

Display current date and time (see expected result for the format)

### Expected Result

```text
Current date is 2020-08-31, 13:20
```


## Exercise 7

### Description

Create a class `Exercise7` in `exercise7.class.php`, import it in `exercises.class.php` and use it

Exercise7 must include a toString function returning `Exercise 7 ok`

### Expected Result

```text
Exercise 7 ok
```


## Exercise 8

### Description

Use [switch / case](https://www.php.net/manual/en/control-structures.switch.php) to map following fruits (`orange`, `lemon`, `raspberry`) to their colors. If fruit not found, return `Fruit not found`

Also format the incoming text to be sure the input and output are as expected

Fill function `exercise8switch` for this exercise

### Expected Result

```text
Raspberry is pink
Lemon is yellow
Apple: Fruit not found
```


## Exercise 9

### Description

Same as exercise 8, but without switch and if / else  
Calling exercise 8 is, of course, forbidden  
Fill function `exercise9noswitch` for this exercise

### Expected Result

```text
Raspberry is pink
Lemon is yellow
Apple: Fruit not found
```


## Exercise 10

### Description

Use exercise 9 to find the color of the fruit given as `fruit` a query parameter  
If no query parameter given, return `No 'fruit' query parameter found`

### Expected Result

If URL is `http://localhost?fruit=lemon`:
```text
Lemon is yellow
```

If URL is `http://localhost?fruit=apple`:
```text
Apple: Fruit not found
```

If URL is `http://localhost?fruity=rlplayer`:
```text
No 'fruit' query parameter found
```

etc...


## Exercise 11

### Description

Use exercise 9 to find the color of the fruit given as `fruit` part of the URI  
If no query parameter given, return `Not a 'fruit' URL`

### Expected Result

If URL is `http://localhost/fruit/lemon`:
```text
Lemon is yellow
```

If URL is `http://localhost/fruit/apple`:
```text
Apple: Fruit not found
```

If URL is `http://localhost/fruity/rlplayer`:
```text
Not a 'fruit' URL
```

etc...


## Bonus exercise

### Description

Thanks to the exercises above (mainly exercise 11), start to think about a routing system, calling specific actions as:  
`http://localhost/external-class/action`  
First part of the URI would be the filename of a class, second would be the function to call

- Firstly, create a `controllers` folder at the root of your project and add a `todo.class.php`
- Secondly, create a `TodoController` class and add a `listAction` function in the class that returns `echo 'Todo list'`
- Finally, create the dynamic routing system to access to the `listAction` if URL = `http://localhost/todo/list`

Tips:

- Nginx is configured to always point index.php, no matter what's after the base url
- Start from a fresh index.php
